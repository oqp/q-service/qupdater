const zookeeper = require('./src/config/zookeeperClient')
const logger = require('./src/logger')
const initialService = require('./src/services/initial')
const updaterService = require('./src/services/updater')
const Transaction = require('./src/utils/Transaction')
const { initialLeaderElection } = require('./src/services/leaderElection')
const { intervalScheduleQueue } = require('./src/services/scheduler')

logger.info('Application Started')
const client = zookeeper.getClient()

logger.info('Connecting to zookeeper')
client.connect()

const getRandomInt = (min, max) => {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min
}

client.once('connected', async () => {
  logger.info('Connected to zookeeper')
  initialService.createInitialNode(client)
  updaterService.intervalUpdateQueue(client, getRandomInt(1000, 3000))
})

client.once('disconnected', async () => {
  logger.info('Disconnected from zookeeper')
  process.exit()
})

process.on('SIGINT', (signal) => {
  const checkFunction = () => {
    if (Transaction.getRunningTaskNumber() === 0) {
      logger.info(`Running Task: ${Transaction.getRunningTaskNumber()}, Clean Exit`)
      process.exit(0)
    } else {
      setTimeout(() => {
        checkFunction()
      }, 50)
    }
  }
  checkFunction()
})