require('dotenv').config()

const logger = require('../logger')
const zookeeperClient = require('../config/zookeeperClient')

const baseQueueOnlinePath = process.env.ZK_QUEUE_BASE_ONLINE_PATH
const baseQueueProcessingPath = process.env.ZK_QUEUE_BASE_PROCESSING_PATH

const removeMillisPrefix = (nodeName) => {
    return nodeName.split(/((\d*)-)(.*)/g)[3]
}

const addMillisPrefix = (queueName) => {
    return `${new Date().getTime()}-${queueName}`
}

const selectAvailableNode = async (client) => {
    const selectNodeStart = new Date()
    const children = await zookeeperClient.getChildren(client, baseQueueOnlinePath)
    if(children.length == 0) {
        logger.info(null, {
            logOrigin: 'queue updater',
            zookeeperFetchQueueTime: new Date() - selectNodeStart,
            unit: 'ms',
        })
        return null
    } else {
        const selectedNode = children.sort()[0]
        logger.debug(`Selected Node: ${selectedNode}`)
        const selectedNodePath = `${baseQueueOnlinePath}/${selectedNode}`
        const selectedNodeData = await getNodeData(client, selectedNodePath)
        const selectedNodeDataBuffer = Buffer.from(selectedNodeData, 'utf-8')
        const selectedNodeDataObj = JSON.parse(selectedNodeData.toString())
        const selectedNodeNameWithoutMillisPrefixAndPath = removeMillisPrefix(selectedNode)
        
        const processingZKPath = `${baseQueueProcessingPath}/${selectedNodeNameWithoutMillisPrefixAndPath}`
        logger.debug(`${baseQueueProcessingPath}/${selectedNodeNameWithoutMillisPrefixAndPath}`)
        if (await isExist(client, processingZKPath) === false) {
            await removeOnlineNode(client, selectedNode)
            await addProcessingNode(client, selectedNodeNameWithoutMillisPrefixAndPath, selectedNodeDataBuffer)
        } else {
            logger.info(`Queue $${selectedNodeNameWithoutMillisPrefixAndPath} is already processing`)
        }

        logger.info(null, {
            logOrigin: 'queue updater',
            zookeeperSelectNodeTime: new Date() - selectNodeStart,
            unit: 'ms',
        })
        return selectedNodeDataObj
    }
}

const setNodeToOnline = async (client, processingNodename) => {
    const processingNodePath = `${baseQueueProcessingPath}/${processingNodename}`
    logger.debug(processingNodePath)

    const selectedNodeData = await getNodeData(client, processingNodePath)
    const selectedNodeDataBuffer = Buffer.from(selectedNodeData, 'utf-8')

    await removeProcessingNode(client, processingNodename)
    await addOnlineNode(client, addMillisPrefix(processingNodename), selectedNodeDataBuffer)
}

const getNodeData = async (client, path) => {
    return await zookeeperClient.getData(client, path)
}

const addOnlineNode = async (client, nodeName, dataBuffer) => {
    const path = `${baseQueueOnlinePath}/${nodeName}`
    await zookeeperClient.create(client, path)
    await zookeeperClient.setData(client, path, dataBuffer)
    return
}


const removeOnlineNode = async (client, nodeName) => {
    await zookeeperClient.remove(client, `${baseQueueOnlinePath}/${nodeName}`)
}

const addProcessingNode = async (client, nodeName, dataBuffer) => {
    const path = `${baseQueueProcessingPath}/${nodeName}`
    await zookeeperClient.create(client, path)
    await zookeeperClient.setData(client, path, dataBuffer)
}

const removeProcessingNode = async (client, nodeName) => {
    await zookeeperClient.remove(client, `${baseQueueProcessingPath}/${nodeName}`)
}

const isExist = async (client, path) => {
    const selectedNode = await zookeeperClient.exist(client, path)
    return selectedNode ? true : false
}

module.exports = {
    isExist,
    selectAvailableNode,
    setNodeToOnline,
    removeProcessingNode,
}