require('dotenv').config()
const zookeeper = require('node-zookeeper-client')
const zookeeperClient = require('../config/zookeeperClient')
const { CreateMode } = zookeeper
const logger = require('../logger')

global.isLeader = false

const ZK_LEADER_ELECTION_PATH = process.env.ZK_LEADER_ELECTION_PATH

const initialLeaderElection = async (client) => {
    const myNodePath = await registerNode(client)
    await checkLeader(client, myNodePath)
}

const registerNode = async (client) => {
    const isExist = await zookeeperClient.exist(client, ZK_LEADER_ELECTION_PATH)
    if (isExist === null || isExist === undefined) {
        await zookeeperClient.create(client, ZK_LEADER_ELECTION_PATH)
    }
    return await zookeeperClient.create(client, `${ZK_LEADER_ELECTION_PATH}/updater-`, null, undefined, CreateMode.EPHEMERAL_SEQUENTIAL)
}

const checkLeader = async (client, myNodePath) => {
    const childrens = await zookeeperClient.getChildren(client, ZK_LEADER_ELECTION_PATH)
    const sortedChildren = childrens.sort()
    const masterNode = sortedChildren[0]
    if (myNodePath.match(masterNode)) {
        global.isLeader = true
        logger.info('I\'m leader')
    } else {
        global.isLeader = false
        logger.info(`Master node is ${masterNode}`)
        const previousSequenceNodeName = sortedChildren[sortedChildren.length - 2]
        zookeeperClient.exist(client, `${ZK_LEADER_ELECTION_PATH}/${previousSequenceNodeName}`, (event) => {
            if (event.name === 'NODE_DELETED') {
                checkLeader(client, myNodePath)
            }
        })
    }
}

module.exports = {
    initialLeaderElection
}