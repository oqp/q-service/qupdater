require('dotenv').config()

const logger = require('../logger')
const zookeeperClient = require('../config/zookeeperClient')

const checkAndCreateNode = async (client, path) => {
    const pathNode = await zookeeperClient.exist(client, path)
    if (pathNode == null) {
        logger.info(`No node creating node: ${path}`)
        await zookeeperClient.create(client, path)
    }
}

const createInitialNode = async (client) => {
    const initialNode = [
        process.env.ZK_QUEUE_BASE_PATH,
        process.env.ZK_QUEUE_BASE_ONLINE_PATH,
        process.env.ZK_QUEUE_BASE_PROCESSING_PATH,
    ]

    initialNode.forEach((item) => {
        checkAndCreateNode(client, item)
    })
}

module.exports = {
    createInitialNode,
}