require('dotenv').config()

const { EventModel } = require('../model/event')
const { audienceSchema } = require('../model/audience')
const { mongoose } = require('../config/mongoose')
const Transaction = require('../utils/Transaction')

const logger = require('../logger')
const AudienceStatus = require('../config/AudienceStatus')

const { setNodeToOnline, selectAvailableNode, removeProcessingNode } = require('./zookeeper')

const updateQueue = async (client) => {
    const transaction = new Transaction()
    transaction.start()
    try {
        let event = null
        const startTime = new Date()
        const selectedQueueData = await selectAvailableNode(client)
        if (selectedQueueData !== null) {
            const { event_uuid: eventUUID } = selectedQueueData
            try {
                logger.debug('selectedQueueData: ' + selectedQueueData)
                if (selectedQueueData == null) return
                event = await EventModel.findOne({ uuid: eventUUID })
                const { currentQueueNumber, sessionTime, maxOutflowAmount, isActive, arrivalTime } = event
    
                const connection = mongoose.connection.useDb(`event-${eventUUID}`)
                const TempAudienceModel = connection.model('Audience', audienceSchema)
                const clearTimestamp = new Date()
                const clearArrivalTimestampComparator = new Date(clearTimestamp.getTime() - (arrivalTime * 1000))
                // Update logic
                // Start Check timeout eligible user (Arrival-Timeout)
                await TempAudienceModel.updateMany({
                    status: AudienceStatus.ELIGIBLED,
                    eligibledAt: {
                        $lt: clearArrivalTimestampComparator
                    }
                }, {
                    status: AudienceStatus.ARRIVAL_TIMEDOUT,
                    arrivalTimedoutAt: clearTimestamp
                })
                // End Check timeout eligible user (Arrival-Timeout)
                // Start Check timeout Serving user
                const clearSessionTimestampComparator = new Date(clearTimestamp.getTime() - (sessionTime * 1000))
                await TempAudienceModel.updateMany({
                    status: AudienceStatus.SERVING,
                    servedAt: {
                        $lt: clearSessionTimestampComparator
                    }
                }, {
                    status: AudienceStatus.SESSION_TIMEDOUT,
                    sessionTimedoutAt: clearTimestamp
                })
                // End Check timeout Serving user
                const servingAudienceAmount = await TempAudienceModel.countDocuments({ status: AudienceStatus.SERVING })
                const eligibledAudienceAmount = await TempAudienceModel.countDocuments({ status: AudienceStatus.ELIGIBLED })
                if (isActive) {
                    // Start calculate how many will call
                    let availableQueueSlot = maxOutflowAmount - (servingAudienceAmount + eligibledAudienceAmount)
                    if (availableQueueSlot < 0) {
                        availableQueueSlot = 0
                    }
                    // End calculate how many will call
                    logger.debug(`availableQueueSlot = ${availableQueueSlot}`, {
                        eventUUID,
                    })
                    // Start update eligible user
                    let callQueueUntil = currentQueueNumber + availableQueueSlot
                    const latestQueueNumber = (await TempAudienceModel.findOne({}, null, { sort: { queueNumber: -1 }})).queueNumber
                    if (callQueueUntil >= latestQueueNumber) {
                        callQueueUntil = latestQueueNumber
                    }
        
                    event.currentQueueNumber = callQueueUntil
                    await EventModel.updateOne({uuid: eventUUID}, event)
                    
                    logger.debug(`next queue number = ${callQueueUntil}`, {
                        eventUUID,
                    })
                    
                    const eligibledAtTimestamp = new Date()
                    const updatedAudiences = (
                        await TempAudienceModel.updateMany({
                            queueNumber: {
                                $gt: currentQueueNumber,
                                $lte: callQueueUntil
                            }
                        }, {
                            status: AudienceStatus.ELIGIBLED,
                            eligibledAt: eligibledAtTimestamp
                        })
                    ).nModified
                    // End update eligible user
                    logger.info('updated audience(s)', {
                        logOrigin: 'queue updater',
                        topic: 'Updated Audience',
                        value: updatedAudiences,
                        eventUUID: eventUUID,
                    })
                    logger.debug('current queue number', {
                        logOrigin: 'queue updater',
                        topic: 'Current Queue Number',
                        value: callQueueUntil,
                        eventUUID: eventUUID,
                    })
                } else {
                    const activeAudience = servingAudienceAmount + eligibledAudienceAmount
                    if (activeAudience === 0) {
                        await removeProcessingNode(client, `event-${selectedQueueData.event_uuid}`)
                        event.isScheduled = false
                        await EventModel.updateOne({ uuid: event.uuid }, event)
                    }
                }
            } catch (err) {
                logger.error(err.toString())
            } finally {
                if (event !== null && event !== undefined && event.isScheduled) {
                    await setNodeToOnline(client, `event-${selectedQueueData.event_uuid}`)
                    logger.debug(`event ${selectedQueueData.event_uuid} set to online`)
                }
            }
            const endTime = new Date()
            logger.info('update duration', {
                logOrigin: 'queue updater',
                queueUpdateDuration: endTime - startTime,
                eventUUID: eventUUID,
                unit: 'ms'
            })
        }
    } catch (err) {
        logger.error(err.toString())
    } finally {
        transaction.end()
    }
    return
}

const intervalUpdateQueue = async (client, timeout) => {
    setTimeout(async () => {
        logger.debug('doing')
        updateQueue(client)
        intervalUpdateQueue(client, timeout)
    }, timeout)
    return
}

module.exports = {
    updateQueue,
    intervalUpdateQueue
}