const { EventModel } = require('../model/event')
const { setNodeToOnline } = require('../services/zookeeper')
const logger = require('../logger')
const Transaction = require('../utils/Transaction')

const intervalScheduleQueue = async (zookeeperClient, interval) => {
    setTimeout(() => {
        scheduleQueue(zookeeperClient, interval)
    }, interval)
}

const scheduleQueue = async (zookeeperClient, interval) => {
    setTimeout(async () => {
        if (global.isLeader) {
            const transaction = new Transaction()
            transaction.start()
            logger.info('Schedule Active')
            const selectActiveEvent = await EventModel.find({
                $or: [
                    {
                        isActive: true,
                        isScheduled: false
                    },
                    {
                        isActive: true,
                        isScheduled: null
                    }
                ]
            })
            selectActiveEvent.forEach(async (event) => {
                await setNodeToOnline(zookeeperClient, `event-${event.uuid}`)
                event.isScheduled = true
                await EventModel.updateOne({ uuid: event.uuid }, event)
            })
            transaction.end()
        }
        scheduleQueue(zookeeperClient, interval)
    }, interval)
}

module.exports = {
    intervalScheduleQueue
}