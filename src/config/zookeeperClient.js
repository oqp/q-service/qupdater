require('dotenv').config()
const zookeeper = require('node-zookeeper-client')
const logger = require('../logger')

const client = zookeeper.createClient(`${process.env.ZK_HOST}`)

const getClient = () => {
    return client
}

const exist = (client, path, watcher) => {
    return new Promise((resolve, reject) => {
        if (client == null) {
            reject('No client specify')
        } else {
            client.exists(path, watcher, (err, stat) => {
                if(err) reject(err)
                else resolve(stat)
            })
        }
    })
}

const getChildren = (client, path, watcher) => {
    logger.debug('getting node\'s children')
    return new Promise((resolve, reject) => {
        if (client == null) {
            reject('No client specify')
        } else {
            client.getChildren(path, watcher, (err, children, stats) => {
                if (err) reject(err)
                else {
                    resolve(children)
                }
            })
        }
    })
}

const create = (client, path, data, acl, mode) => {
    logger.debug('creating node')
    return new Promise((resolve, reject) => {
        if (client == null) {
            reject('No client specify')
        } else {
            client.create(path, data, acl, mode, (err, path) => {
                if (err) reject(err)
                else resolve(path)
            })
        }
    })
}

const remove = (client, path, version) => {
    logger.debug('removing node')
    return new Promise((resolve, reject) => {
        if (client == null) {
            reject('No client specify')
        } else {
            if (version) {
                client.remove(path, version, (err) => {
                    if (err) reject(err)
                    else resolve()
                })
            } else {
                client.remove(path, (err) => {
                    if (err) reject(err)
                    else resolve()
                })
            }
        }
    })
}

const getData = (client, path, watcher) => {
    logger.debug('getting node\'s data')
    return new Promise((resolve, reject) => {
        if (client == null) {
            reject('No client specify')
        } else {
            client.getData(path, watcher, (err, data, stat) => {
                if (err == null) {
                    resolve(data)
                } else {
                    reject(err)
                }
            })
        }
    })
}

const setData = (client, path, data) => {
    logger.debug('setting node\'s data')
    return new Promise((resolve, reject) => {
        if (client == null) {
            reject('No client specify')
        } else {
            client.setData(path, data, (err, data) => {
                if (err == null) {
                    resolve(data)
                } else {
                    reject(err)
                }
            })
        }
    })
}

module.exports = {
    getClient,
    exist,
    getChildren,
    create,
    remove,
    getData,
    setData
}