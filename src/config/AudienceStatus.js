class AudienceStatus {
  static get COMPLETED() {
    return 'completed'
  }

  static get WAITING() {
    return 'waiting'
  }

  static get SERVING() {
    return 'serving'
  }

  static get ELIGIBLED() {
    return 'eligibled'
  }

  static get ARRIVAL_TIMEDOUT() {
    return 'arrival-timedout'
  }

  static get SESSION_TIMEDOUT() {
    return 'session-timedout'
  }

  static get CANCELED() {
    return 'canceled'
  }
}

module.exports = AudienceStatus