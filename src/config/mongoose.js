require('dotenv').config({ path: '/' })

const logger = require('../logger')

const mongoose = require('mongoose')

mongoose.set('bufferCommands', false)
mongoose.set('useFindAndModify', false)
mongoose.set('useCreateIndex', true)
mongoose.connect(`mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`, { useNewUrlParser: true, connectTimeoutMS: 2000 })

const db = mongoose.connection
db.on('connecting', () => {
    logger.info('Connecting to mongodb server')
})
db.on('error', () => {
    logger.error('Cannot connect to mongodb server')
})
db.once('open', () => {
    logger.info('Connected to mongodb server')
})

module.exports = { db, mongoose }