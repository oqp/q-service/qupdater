const Hashmap = require('hashmap')

let transactionIdCounter = 0
const runningTransactionTicketMap = new Hashmap()

class Transaction {
  constructor() {
    this.transactionId = transactionIdCounter++
  }

  start() {
    runningTransactionTicketMap.set(this.transactionId)
  }

  end() {
    runningTransactionTicketMap.remove(this.transactionId)
  }

  static getRunningTaskNumber() {
    return runningTransactionTicketMap.count()
  }
}
module.exports = Transaction