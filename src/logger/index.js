/* eslint-disable no-param-reassign */
require('dotenv').config()
const winston = require('winston')

const MESSAGE = Symbol.for('message')

const jsonFormatter = (logEntry) => {
  const base = { timestamp: new Date() }
  const json = Object.assign(base, logEntry)
  logEntry[MESSAGE] = JSON.stringify(json)
  return logEntry
}

const { format } = winston

const logger = winston.createLogger({
  level: process.env.LOG_LEVEL || 'info',
  format: format(jsonFormatter)(),
  transports: [
    new winston.transports.Console({
      handleExceptions: true,
    }),
  ],
})

module.exports = logger
