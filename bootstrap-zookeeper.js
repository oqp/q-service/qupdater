require('dotenv').config()

const zookeeper = require('./src/config/zookeeperClient')
const { createInitialNode } = require('./src/services/initial')
const logger = require('./src/logger')

logger.info('Application Started')
const client = zookeeper.getClient()

logger.info('Connecting to zookeeper')
client.connect()

client.once('connected', async () => {
    logger.info('Connected to zookeeper')
    logger.info(`eventUUID ${process.argv[2]}`)

    createInitialNode(client)

    try {
        await zookeeper.create(client, `${process.env.ZK_QUEUE_BASE_ONLINE_PATH}/${new Date().getTime()}-event-${process.argv[2]}`, Buffer.from(JSON.stringify({ 'event_uuid': process.argv[2] }), 'utf-8'))
    } catch (e) {
        await zookeeper.remove(client, `${process.env.ZK_QUEUE_BASE_PROCESSING_PATH}/event-${process.argv[2]}`)
        logger.error(e)
    }
    process.exit()
})
